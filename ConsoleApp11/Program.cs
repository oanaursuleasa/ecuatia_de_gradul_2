﻿using System;

namespace ConsoleApp11
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, c;
            Console.WriteLine("Introduceti a= ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduceti b=");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduceti c=");
            c = int.Parse(Console.ReadLine());

            float delta = b * b - 4 * a * c;

            double x1;
            double x2;

            if (delta>=0)
            {
                if(delta==0)
                {
                    x1 = -b / (2 * a);
                    Console.WriteLine(" Ecuatia are 2 solutii reale egale : " + x1 );

                }
                else
                {
                    x1 = (-b + Math.Sqrt(delta)) / (2 * a);
                    x2 = (-b - Math.Sqrt(delta)) / (2 * a);
                    Console.WriteLine("Ecuatia are 2 solutii reale egale : " + x1  + " si " + x2 );
                }
            }

            Console.ReadKey();
        }
    }
}
